class Students:
    School="SVN"

    def __init__(self,m1,m2,m3):
        self.m1=m1
        self.m2=m2
        self.m3=m3
    def avg(self):
        return (self.m1+self.m2+self.m3)/3
    @classmethod
    def info(cls):
        return cls.School
    @staticmethod
    def expo():
        print("This is student Class in ABC Module")
Students.expo()
s1=Students(34,35,36)
s2=Students(66,77,88)
print(s1.avg())
res=s2.avg()
print(res)
print(Students.info())