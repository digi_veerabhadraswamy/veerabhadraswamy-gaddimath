class Students:
    def __init__(self,name,rollnum):
        self.name=name
        self.rollnum=rollnum
        self.lap=self.Laptop()
    def show(self):
        print(self.name,self.rollnum)

    class Laptop:
        def __init__(self):
            self.brand="HP"
            self.cpu="i5"
            self.ram="8GB"
        def show(self):
            print(self.brand,self.cpu,self.ram)
s1=Students("VSG","2RH10ME090")
s2=Students("VMC","2RH10CS100")
s1.show()
Lap1=Students.Laptop()
Lap2=Students.Laptop()
Lap1.show()
s2.show()
Lap2.show()