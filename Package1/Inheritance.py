class A:
    def __init__(self):
        print("__init__ in class A")
    def feature1(self):
        print("Feature 1 is working")
    def feature2(self):
        print("Feature 2 is working")
class B():
    def __init__(self):
        print("__init__ in class B")
    def feature3(self):
        print("Feature 3 is working")
    def feature4(self):
        print("Feature 4 is working",)

class C(A,B):
    def __init__(self):
        #print("Feature 5 is working")
        print("__init__ in class C")
        super().__init__()
    def feat(self):
        super().feature4()
OC=C()
OC.feat()