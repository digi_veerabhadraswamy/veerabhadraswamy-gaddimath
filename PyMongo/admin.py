from pymongo import MongoClient
from bson.objectid import ObjectId
from random import randint
# client = MongoClient()
from flask import Flask, jsonify, request

app = Flask(__name__)
client = MongoClient('mongodb://localhost:27017/')
# db=client.test_database
db = client['empDb']
collection = db['Emp']
Emp = db.Emp

@app.route('/create_doc', methods=['POST'])
def create_doc():
    # for i in range(20):
    #     a = {'name': 'Veera' + str(i), 'age': randint(20, 60), 'Fees': randint(20000, 200000)}
    input = request.json
    data = {
        "name" : input.get('name'),
        "age" : input.get('age'),
        "gender": input.get('sex')
    }
    doc = Emp.insert_one(data)
    return jsonify({
        'message': "Success"
    })
@app.route('/read_doc')
def read_doc(db):
    task1 = db.Emp.find({"age": {"$gt": 20}})
    task2 = db.Emp.find({"age": {"$lt": 40}})
    task3 = db.Emp.find({"age": {"$gt": 20, "$lt": 45}})
    task4 = db.Emp.find({"age": {"$gt": 20, "$lt": 45}, "salary": {"$gt": 34000}})
    def Retrive_data(*args):
        # lst=[task1,task2,task3,task4]
        for x in args:
            for Emp in x:
                print(Emp)
                print("*" * 56)
if __name__ == "__main__":
    app.run(debug=True)