from pymongo import MongoClient
from bson import ObjectId , json_util
from random import randint
# client = MongoClient()
from flask import Flask, jsonify, request,json
from flask import render_template
app = Flask(__name__)
client = MongoClient('mongodb://localhost:27017/')
# db=client.test_database
db = client['empDb']
collection = db['Emp']
Emp = db.Emp
@app.route('/create_doc', methods=['POST'])
def create_doc():
    Input = request.json
    print("Input: ", Input)
    # data = {
    #     "name" : input.get('name'),
    #     "age" : input.get('age'),
    #     "gender": input.get('sex')
    # }
    # for data in Input :
    # import pdb;pdb.set_trace()
    x = Emp.insert_many(Input['insert_json'])
    # x = Emp.insert_many(Input)
    print("Document : ",x)
    return jsonify({
     'message': "Success"
    })
@app.route("/retrieve_doc", methods=['GET'])
def retrieve_doc():
    cursor=Emp.find({})
    doc= list()
    for document in cursor:
        # print(document)
        # doc.update(document)
        document['_id'] = str(document['_id'])
        doc.append(document)
    # import pdb;pdb.set_trace()
    return jsonify({
        'all_data': doc
    })
@app.route("/retrieve_one", methods=['POST'])
def retrieve_one():
    data = request.json
    Emp.update({'_id':ObjectId(data['user_id'])},{'$set':{'age':38}})
    cursor=Emp.find_one({"_id": ObjectId(data['user_id'])})

    cursor['_id'] = str(cursor['_id'])
    return jsonify({
        'user_details': cursor
    })
@app.route("/delete_user", methods=['DELETE'])
def delete_user():
    data= request.json
    db.Emp.delete_one({'_id':ObjectId(data['user_id'])})
    return jsonify({
        'message': 'user deleted successfully'
    })

if __name__ == "__main__":
    app.run(debug=True)

print("CRUD Operations Successfully Done")